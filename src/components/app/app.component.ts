import { Component } from '@angular/core';

import '../../public/css/styles.css';

import { HeaderComponent } from '../header';
import { FooterComponent } from '../footer';

@Component({
  selector: 'app',
  template: require('./app.html'),
  directives: [HeaderComponent, FooterComponent]
})

export class AppComponent { }
