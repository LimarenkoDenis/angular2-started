import { Component } from '@angular/core';

@Component({
  selector: 'ng-footer',
  template: require('./footer.html')
})
export class FooterComponent {};
